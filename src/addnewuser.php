<?php
if (@!session_id()) session_start();

if (@!$_SESSION['token']) {
    @header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}

// if (@!$_POST['username'] || @!$_POST['password']) {
//     header("Location: usermanager.php");
//     echo "<script>location='usermanager.php'</script>";
//     exit;
// }

$name = @$_POST['add-displayname'];
$username = @$_POST['add-username'];
$password = @$_POST['add-password'];
$fromfaculty = @$_POST['add-fromfaculty'];
$role_id = @$_POST['add-role'];

require_once "api_setting.php";
require_once "functions.php";

$url = $apiUrl."/register";
$data = array("name"=>$name, "username"=>$username, "password"=>$password, "from_faculty"=>$fromfaculty, "role_id"=>$role_id);
$data = json_encode($data);

$response = CallAPI('POST', $url, $data);

var_dump($response);
exit;

if (@!$response) {
    header("Location: operationincompleted.php");
    echo "<script>location='operationincompleted.php'</script>";
    exit;
} 

@header("Location: usermanager.php");
echo "<script>location='usermanager.php'</script>";
exit;
