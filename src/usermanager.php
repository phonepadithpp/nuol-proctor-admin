<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token'] && @$_SESSION['role_id'] != 1) {
    @header("Location: login.php");
    echo "<script>location='login.ph'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";

// $apiUrl = "127.0.0.1:8000/api";
$url = $apiUrl . "/listproctors";

$response = json_decode(CallAPI('GET', $url), true);
if (!$response) {
    echo "Can not connect to database: the connection error";
    exit;
}
$data = @$response['data'] ? $response['data'] : array();
// print_r($data);
// exit;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css" />
    <link href="css/custom.css" rel="stylesheet">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.js"></script>

    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link active" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                        </li>
                    <?php } ?>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                        </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i> <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php"><i class="fa fa-unlock-alt"></i> ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="navbar navbar-expand-sm">
            <a href="#" class="nav-link text-body">ຈັດການບັນຊີຜູ້ໃຊ້</a><i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
        <div class="row d-flex my-3">
            <table class="table table-hover" id="usertable" style="width:100%">
                <thead>
                    <tr>
                        <th>ຊື່ນາມສະກຸນ</th>
                        <th>ຈາກພາກສ່ວນ</th>
                        <th>ຫ້ອງ</th>
                        <th>ຊື່ບັນຊີຜູ້ໃຊ້</th>
                        <th>ປະເພດ</th>
                        <th style="width:120px"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // print_r($data);
                    foreach ($data as $i => $user) {
                        if ($user['username'] == "admin") continue;
                    ?>
                        <tr>
                            <td><?php echo @$user['name'] ?></td>
                            <td><?php echo @$user['from_faculty'] ?></td>
                            <td>
                                <?php
                                
                                if (@$user['proctor_room']) {
                                    foreach (@$user['proctor_room'] as $room) {
                                ?>
                                        <span class='btn btn-update badge bg-primary btn-sm' data-bs-toggle="modal" data-bs-target="#proctorroom" data-id="<?php echo @$user['proctor_id'] ?>" data-toggle="tooltip" data-placement='top' title="<?php echo "$room[room_name], $room[site_name]" ?>"><?php echo $room['room_name'] ?></span>
                                    <?php
                                    }
                                } else {
                                    ?>
                                    <span class='btn btn-update badge bg-warning' data-bs-toggle="modal" data-bs-target="#proctorroom" data-id="<?php echo @$user['proctor_id'] ?>"><i class='fa fa-plus-square'></i></span>
                                <?php
                                }
                                ?>
                            </td>
                            <td><?php echo @$user['username'] ?></td>
                            <td><?php echo @$user['role_name'] ?></td>
                            <td class="text-center">
                                <button type="button" name="change-password" class="btn btn-sm btn-secondary" data-bs-toggle="modal" data-bs-target="#changePassword" data-id="<?php echo @$user['proctor_id'] ?>" data-user="<?php echo @$user['username'] ?>" data-toggle="tooltip" data-placement="top" title="Rest Password">
                                    <i class="fa fa-unlock-alt"></i>
                                </button>
                                <button class="btn btn-success btn-sm" name="edit-user" type="button" data-bs-toggle="modal" data-bs-target="#EditUser" data-id="<?php echo @$user['proctor_id'] ?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" name="delete-user" type="button" data-bs-toggle="modal" data-bs-target="#DeleteUser" data-id="<?php echo @$user['proctor_id'] ?>" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr style="border-color:aliceblue">
                        <td colspan="5"></td>
                        <td class="text-center">
                            <button class="btn btn-primary btn-sm" name="add-user" type="button" data-bs-toggle="modal" data-bs-target="#AddUser" data-toggle="tooltip" data-placement="top" title="Add New User"><i class="fa fa-plus"></i> ເພີ່ມບັນຊີຜູ້ໃຊ້</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="row d-flex flex-row">
            <hr>
            <footer>
                <div class="col text-center">
                    <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                </div>
                <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
            </footer>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal fade" id="changePassword">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ຕັ້ງລະຫັດຜ່ານໃໝ່</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center m-4">
                    <div>ຊື່ບັນຊີຜູ້ໃຊ້: <strong><span id="modal-username" class="text-bold"></span></strong></div>
                    <div class="form-group">
                        <input type="hidden" name="id" id="modal-id">
                        <label for="password">ລະຫັດຜ່ານໃໝ່:</label>
                        <input type=text class="form-control m-2 text-center text-primary" name="modal-password" id="modal-password" required>
                    </div>
                    <div class="text-center"><button name="password-gen" type="button" class="btn btn-sm btn-secondary">Password Generater</button></div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" name="save-password" data-bs-dismiss="modal">ບັນທຶກ</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">ປິດ</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="AddUser">
        <div class="modal-dialog modal-dialog-centered">
            <!-- <form method="post" action="addnewuser.php" style="width: 100%;"> -->
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ເພີ່ມຂໍ້ມູນບັນຊີຜູ້ໃຊ້ງາານໃໝ່</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body m-4">
                    <div class="form-group">
                        <label for="add-displayname">ຊື່ແລະນາມສະກຸນ:</label>
                        <input type=text class="form-control m-2" name="add-displayname" id="add-displayname" required>
                    </div>
                    <div class="form-group">
                        <label for="add-fromfaculty">ຈາກພາກສ່ວນ:</label>
                        <input type=text class="form-control m-2" name="add-fromfaculty" id="add-fromfaculty">
                    </div>
                    <div class="form-group">
                        <label for="add-username">ຊື່ບັນຊີຜູ້ໃຊ້:</label>
                        <input type=text class="form-control m-2" name="add-username" id="add-username" required>
                    </div>
                    <div class="form-group">
                        <label for="add-password">ລະຫັດຜ່ານ:</label>
                        <div class="input-group">
                            <input type=text class="form-control" name="add-password" id="add-password" required>
                            <button name="password-gen" type="button" class="btn btn-secondary">Gen</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-role">ປະເພດບັນຊີ:</label>
                        <select class="form-select m-2" name="add-role" id="add-role">
                        </select>
                    </div>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" name="submit-add-user">ບັນທຶກ</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">ປິດ</button>
                </div>

            </div>
            <!-- </form> -->
        </div>
    </div>
    <div class="modal fade" id="EditUser">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ແກ້ໄຂຂໍ້ມູນບັນຊີຜູ້ໃຊ້ງາານ</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body m-4">
                    <input type="hidden" name="id" id="edit-id">
                    <div class="form-group">
                        <label for="edit-displayname">ຊື່ແລະນາມສະກຸນ:</label>
                        <input type=text class="form-control m-2" name="edit-displayname" id="edit-displayname" required>
                    </div>
                    <div class="form-group">
                        <label for="edit-fromfaculty">ຈາກພາກສ່ວນ:</label>
                        <input type=text class="form-control m-2" name="edit-fromfaculty" id="edit-fromfaculty">
                    </div>
                    <div class="form-group">
                        <label for="edit-username">ຊື່ບັນຊີຜູ້ໃຊ້:</label>
                        <input type=text class="form-control m-2" name="edit-username" id="edit-username" required>
                    </div>
                    <div class="form-group">
                        <label for="edit-role">ປະເພດບັນຊີ:</label>
                        <select class="form-select m-2" name="edit-role" id="edit-role">
                        </select>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" name="update-user" data-bs-dismiss="modal">ບັນທຶກ</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">ປິດ</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="DeleteUser">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title mx-auto">ລືມບັນຊີຜູ້ໃຊ້ງາານ</h4>
                </div>

                <!-- Modal body -->
                <div class="modal-body m-2">
                    <input type="hidden" name="id" id="delete-id">
                    <div class="p-3 alert alert-danger" id="delete-alert"></div>
                    <div class="text-center">ກະລຸນາຢືນຢັນ ທ່ານຕ້ອງການລືມບັນຊີຜູ້ໃຊ້ນີ້ອອກ</div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-success" name="comfirm-delete">ຢືນຢັນການລືມ</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">ຍົກເລີກ</button>
                </div>

            </div>
        </div>
    </div>

    <!-- Assign proctor to exam_room -->
    <div class="modal fade" id="proctorroom--">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ແກ້ໄຂຂໍ້ມູນບັນຊີຜູ້ໃຊ້ງາານ</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <form method="post" --action="changeproctoringroom.php">
                    <div class="modal-body m-4">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="edit-displayname">ຊື່ແລະນາມສະກຸນ:</label>
                            <input type=text class="form-control m-2" name="displayname" id="displayname" required>
                        </div>
                        <div class="form-group">
                            <label for="exam_site">ເລືອກສູນສອບເສັງ:</label>
                            <select class="form-select m-2" name="exam_site" id="exam_site">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exam_building">ເລືອກສູນສອບເສັງ:</label>
                            <select class="form-select m-2" name="exam_building" id="exam_building">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exam_room">ຫ້ອງສອບເສັງ:</label>
                            <select class="form-select m-2" name="exam_room" id="exam_room">
                            </select>
                        </div>
                        <label for="exam_date">ວັນທີ:</label>
                        <div class="input-group">
                            <input type=text class="form-control m-2" name="exam_date" id="exam_date" placeholder="yyyy-mm-dd" required>
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="session1" name="session[]" value="ພາກເຊົ້າ" checked>
                                <label class="form-check-label" for="check1">Option 1</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="session2" name="session[]" value="ພາກບ່າຍ">
                                <label class="form-check-label" for="check2">Option 2</label>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" name="save">ບັນທຶກ</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">ປິດ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>
<session>
    <script src="js/custom.js?<?php echo md5(rand()) ?>"></script>
    <script>
        $("#usertable").DataTable({
            responsive: true,
            paging: true,
            pageLength: 50,
            ordering: false,
            info: false,
            searching: true,
            language: {
                lengthMenu: 'ສະແດງ _MENU_ ລາຍການຕໍ່ໜ້າ',
                zeroRecords: 'ບໍ່ທົບຂໍ້ມູນ',
                search: 'ຄົ້ນຫາ:',
                infoEmpty: 'ບໍ່ມີຂໍ້ມູນສະແດງ',
                paginate: {
                    first: "ທໍາອິດ",
                    last: "ສຸດທ້າຍ",
                    next: "ທັດໄປ",
                    previous: "ກ່ອນໜ້າ"
                },
            },
        });
    </script>
</session>