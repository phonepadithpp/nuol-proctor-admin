<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token']) {
    header("Location: login.php");
    echo "<script>location='login.ph'</script>";
    exit;
}
$token = bin2hex(random_bytes(16));
$_SESSION['verify_token'] = $token;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css?dfd" rel="stylesheet">
    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php $_SESSION['role_id'] != 1 ? "disabled" : "" ?>" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php $_SESSION['role_id'] != 1 ? "disabled" : "" ?>" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php">ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php">ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-12 col-md-8 col-lg-5">
                <div class="card bg-light m-3">
                    <div class="card-body p-5">
                        <form class="mb-3 mt-md-4" method="POST" action="passwordchanger.php">
                            <input type="hidden" name="verify_token" value="<?php echo $token ?>">
                            <h2 class="fw-bold mb-2 text-center">ປ່ຽລະຫັດຜ່ານ</h2>
                            <p class=" mb-5">
                                <?php
                                if (@$_GET['error'] == 100) {
                                    echo "<div class='text-danger'>ລະຫັດຜ່ານປະຈຸບັນບໍ່ຖືກຕ້ອງ! ກະລຸນາລອງໃໝ່</div>";
                                } else if (@$_GET['error'] == 101) {
                                    echo "<div class='text-danger'>ລະຫັດຜ່ານໃໝ່ບໍ່ກົງກັນ! ກະລຸນາລອງໃໝ່</div>";
                                } else if (@$_GET['error'] == 102) {
                                    echo "<div class='text-danger'>ການດໍາເນີນການບໍ່ສໍາເລັດ! ກະລຸນາລອງໃໝ່</div>";
                                }
                                ?></p>
                            <div class="form-group mb-3">
                                <label for="currpassword">ລະຫັດຜ່ານປະຈຸບັນ:</label>
                                <input type="password" name="currpassword" class="form-control" id="currpassword" placeholder="Current Password" required>
                            </div>
                            <div class="form-group mb-3">
                                <label for="newpassword">ລະຫັດຜ່ານໃໝ່:</label>
                                <input type="password" name="newpassword" class="form-control" id="newpassword" placeholder="New Password" required>
                            </div>
                            <div class="form-group mb-3">
                                <label for="repassword">ຢຶນຢັນລະຫັດຜ່ານໃໝ່:</label>
                                <input type="password" name="repassword" class="form-control" id="repassword" placeholder="Confirm New Password">
                            </div>
                            <!-- <p class="small"><a class="text-primary" href="forget-password.html">Forgot password?</a></p> -->
                            <div class="d-grid">
                                <button class="btn btn-secondary mt-5" type="submit">ບັນທຶກ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row d-flex flex-row">
                <hr>
                <footer>
                    <div class="col-12 text-center">
                        <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                    </div>
                    <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
                </footer>
            </div>
        </div>
    </div>
</body>

</html>