<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token']) {
    @header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}

if (@!$_GET['id']) {
    @header("Location: viewsite.php");
    echo "<script>location='viewsite.php'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";

// echo $apiUrl = "127.0.0.1:8000/api";
$url = $apiUrl . "/roomdetail/" . $_GET['id'];

$response = json_decode(CallAPI('GET', $url), true);
if (!$response) {
    echo "Can not connect to database: the connection error";
    exit;
}
$data = @$response['data'] ? $response['data'] : array();

// var_dump($data);
// exit;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css" rel="stylesheet">
    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                        </li>
                    <?php } ?>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                        </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i> <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php"><i class="fa fa-unlock-alt"></i> ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="navbar navbar-expand-sm">
            <a href="main.php" class="nav-link">ໜ້າຫຼັກ</a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="viewsite.php?id=<?php echo @$data['building']['site']['id'] ?>" class="nav-link">ສູນສອບເສັງ: <?php echo @$data['building']['site']['name'] ?></a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="#" class="nav-link text-body">ຫ້ອງສອບເສັງ: <?php echo $data['room_name'] ?></a><i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
        <div class="my-4 rounded-3">
            <h5>ຫ້ອງສອບເສັງ: <span class="badge bg-info"><?php echo @$data["room_name"] ?></span>, ອາຄານ: <?php echo @$data["building"]["name"] ?>, ສູນສອບເສັງ: <?php echo @$data["building"]["site"]["name"] ?></h5>
        </div>
        <div class="row g-3 mb-5">
            <div class="table-responsive">
                <table class="table  table-bordered mt-2" id="total" style="width:100%">
                    <tr>
                        <th colspan="3" rowspan='2' class="text-center align-bottom">
                            <div class="h5"><strong> ລາຍຊື່ນັກສອບເສັງທັງໝົດ</strong></div>
                        </th>
                        <?php
                        $data['sessions'] = (array)$data['sessions'];
                        if (count($data['sessions']) > 0) {
                        ?>
                            <th colspan="<?php echo count((array)$data['sessions']) ?>" class="text-center">ກໍາມະການຍາມຫ້ອງ</th>
                        <?php } else { ?>
                            <th class="text-center">ກໍາມະການຍາມຫ້ອງ</th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php
                        if (count($data['sessions']) > 0) {
                            foreach ($data['sessions'] as $SE) {
                        ?>
                                <td class='text-center'>
                                    <?php
                                    $SE['proctors'] = $SE['proctors'] ? $SE['proctors'] : array();
                                    foreach ((array)$SE['proctors'] as $i => $proctor) {
                                        $i++;
                                        echo "$i. $proctor[name]<br>";
                                    }
                                    ?>
                                </td>
                            <?php }
                        } else { ?>
                            <td class="text-center"> </td>
                        <?php } ?>
                    </tr>
                    <tr class="table-primary">
                        <th class='text-center'>ລ/ດ</th>
                        <th class='text-center'>ເລກບັດ</th>
                        <th>ຊື່ນັກສອບເສັງ</th>
                        <?php
                        if (count($data['sessions']) > 0) {
                        ?>
                            <?php
                            foreach ($data['sessions'] as $SE) {
                                $SE['date'] = date('m/d/y', strtotime(@$SE['date']));
                            ?>
                                <th class='text-center'><?php echo @$SE['date'] ?> (<?php echo @$SE['session'] ?>)</th>
                            <?php } ?>
                        <?php } else { ?>
                            <th class="text-center"> - </th>
                        <?php } ?>
                    </tr>

                    <?php foreach ((array)$data['applicants'] as $i => $val) {
                        $i++;
                        echo "<tr>
                        <td class='text-center'>$i</td>
                        <td class='text-center'>$val[seat_no]</td>
                        <td>$val[full_name]</td>";

                        if (count($data['sessions']) > 0) {
                            foreach ($data['sessions'] as $SE) {
                                echo "<td class='text-center'>";
                                if ($SE['attendances'] == null) {
                                    $SE['attendances'] = array();
                                }
                                $check = false;
                                foreach ((array)$SE['attendances'] as $att) {
                                    if (in_array($val['id'], $att)) {
                                        $check = true;
                                    }
                                }
                                if ($check) {
                                    echo "<i class='fa fa-check text-success'></i>";
                                } else {
                                    // echo "<i class='fa fa-close text-danger'></i>";
                                    echo "";
                                }
                                echo "</td>";
                            }
                        } else {
                            echo "<td></td>";
                        }
                        echo "</tr>";
                    } ?>

                    <tr>
                        <th colspan="3" class="text-end"><strong>ທັງໝົດ <?php echo count((array)$data['applicants']) ?> ຄົນ, ມາ:</strong></th>
                        <?php
                        foreach ($data['sessions'] as $SE) {
                            echo "<th class='text-center'><strong>";
                            echo @$SE['attendances'] ? count($SE['attendances']) : '0';
                            echo "</strong></th>";
                        }
                        ?>
                    </tr>
                </table>
            </div>
            <div class="row d-flex flex-row">
                <hr>
                <footer>
                    <div class="col text-center">
                        <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                    </div>
                    <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
                </footer>
            </div>
        </div>
</body>

</html>
<section>
    <script></script>
</section>