<?php
if (@!session_id()) session_start();
require_once "api_setting.php";
require_once "functions.php";
$userId = $_SESSION['user_id'];
$_SESSION = array();
session_destroy();
CallAPI('POST', $apiUrl."/logging", json_encode(array("event"=>"Logout Success: ok", "proctor_id"=>@$userId)));
@header("Location: login.php");
echo "<script>location='login.php'</script>";