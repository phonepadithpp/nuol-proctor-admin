<?php
if (@!session_id()) session_start();

if (@!$_POST['verify_token'] | (@$_POST['verify_token'] != $_SESSION['verify_token'])) {
    header("Location: changecurrentpassword.php");
    echo "<script>location='changecurrentpassword.php'</script>";
    exit;
}
unset($_SESSION['verify_token']);

if (@!$_SESSION['token']) {
    header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}

if (@!$_POST['currpassword']) {
    header("Location: changecurrentpassword.php?error=100");
    echo "<script>location='changecurrentpassword.php?error=100'</script>";
    exit;
}
if (@$_POST['newpassword']!==@$_POST['repassword']) {
    header("Location: changecurrentpassword.php?error=101");
    echo "<script>location='changecurrentpassword.php?error=101'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";

$passwdchkurl = $apiUrl."/login";
$data = array("username"=>$_SESSION['username'], "password"=>$_POST['currpassword']);
$data = json_encode($data);

$response = json_decode(CallAPI('POST', $passwdchkurl, $data));
if (@!$response->token) {
    header("Location: changecurrentpassword.php?error=100");
    echo "<script>location='changecurrentpassword.php?error=100'</script>";
    exit;
}
$user = token_decode($response->token);

$passwdchurl = $apiUrl."/proctor/update";
$data = json_encode(array("proctor_id"=>@$user['user_id'], "password"=>$_POST['newpassword']));

$response = json_decode(CallAPI('PUT', $passwdchurl, $data));

if (@!$response) {
    header("Location: changecurrentpassword.php?error=102");
    echo "<script>location='changecurrentpassword.php?error=102'</script>";
    exit;
} 

header("Location: operationcompleted.php");
echo "<script>location='operationcompleted.php'</script>";
exit;
