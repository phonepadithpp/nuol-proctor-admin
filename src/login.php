<?php
if (@!session_id()) session_start();
$_SESSION = array();

$token = bin2hex(random_bytes(16));
$_SESSION['verify_token'] = $token;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css" rel="stylesheet">
</head>

<body>
    <div class="vh-100 d-flex justify-content-center align-items-center">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="card bg-light">
                        <div class="border border-3 border-primary bg-primary"></div>
                        <div class="card-body p-5">
                            <form class="mb-3 mt-md-4" method="POST" action="auth_check.php">
                                <input type="hidden" name="verify_token" value="<?php echo $token ?>">
                                <h2 class="fw-bold mb-2 text-center">ລະບົບຕິດຕາມການສອບເສັງເຂົ້າ ມຊ.</h2>
                                <p class=" mb-5">
                                    <?php
                                    if (@$_GET['error'] == 100) {
                                        echo "<div class='text-danger'>ການຢືນຢ້ນບໍ່ຖືກຕ້ອງ! ກະລຸນາລອງໃໝ່</div>";
                                    } else if (@$_GET['error'] == 101) {
                                        echo "<div class='text-danger'>ລະບົບມີບັນຫາ ບໍ່ສາມາດເຊື່ອມຕໍ່ໄດ້ໃນຄະນະນີ້</div>";
                                    }
                                    ?></p>
                                <div class="mb-3">
                                    <label for="username" class="form-label ">ບັນຊີຜູ້ໃຊ້</label>
                                    <input type="text" name="username" class="form-control" id="username" placeholder="Username">
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label ">ລະຫັດຜ່ານ</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <!-- <p class="small"><a class="text-primary" href="forget-password.html">Forgot password?</a></p> -->
                                <div class="d-grid">
                                    <button class="btn btn-secondary" type="submit">ເຂົ້າສູ່ລະບົບ</button>
                                </div>
                            </form>
                            <!-- <div> -->
                            <!-- <p class="mb-0  text-center">Don't have an account? <a href="signup.html" class="text-primary fw-bold">Sign -->
                            <!-- Up</a></p> -->
                            <!-- </div> -->

                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-flex flex-row my-3">
                <footer>
                    <div class="col text-center">
                        <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                    </div>
                    <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
                </footer>
            </div>
        </div>
    </div>
</body>

</html>