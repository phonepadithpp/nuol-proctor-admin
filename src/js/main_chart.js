var ctxB = document.getElementById("overalstatistic").getContext("2d");
var Chart = new Chart(ctxB, {
  type: "bar",
  data: {
    labels: ["ທັງທົດ", "ເຂົ້າເສັງ"],
    datasets: [
      {
        label: "ຈໍານວນນັກສອບເສັງ",
        data: [0, 0],
        fill: false,
        backgroundColor: ["rgba(54, 162, 235, 0.2)", "rgba(255, 99, 132, 0.2)"],
        borderColor: ["rgb(54, 162, 235)", "rgb(255, 99, 132)"],
        borderWidth: 1,
      },
    ],
  },
  options: {
    indexAxis: 'y',
    scales: {
      y: {
            beginAtZero: true,
          },
    },
    plugins: {
      legend: { display: false },
    },
    maintainAspectRatio: false,
    responsive: true,
  },
});

function updateData(chart, labels, newdata) {
  if (labels) chart.data.labels = labels;
  chart.data.datasets.forEach((dataset) => {
    dataset.data = newdata;
  });
  chart.update();
}

// console.log(apiUrl)

var api_url = apiUrl + "/overalstatistic";

$.getJSON(api_url, function (result) {
    // console.log(result);
    var Data = [result.data[0].total, result.data[0].attendance];
    updateData(Chart,null,Data);
  });

setInterval(function () {
  // var dataPoints = [];
  $.getJSON(api_url, function (result) {
    var Data = [result.data[0].total, result.data[0].attendance];
    updateData(Chart,null,Data);
  });
}, 10 * 1000);
