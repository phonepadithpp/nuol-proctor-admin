$(document).ready(function () {
  $(function () {
    $('[data-toggle="popover"]').popover();
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });

  // Change user password
  $('[name="change-password"]').click(function () {
    if (typeof $(this).data("id") !== "undefined") {
      data_id = $(this).data("id");
      data_user = $(this).data("user");
      $("#modal-id").val(data_id);
      $("#modal-username").html(data_user);
      $("#modal-password").val('');
    } else {
      return false;
    }
  });

  $('[name="password-gen"]').click(function () {
    // let randPassword = (Math.random() + 1).toString(36).slice(-8)
    let pLength = 8;
    let randPassword = Array(pLength)
      .fill("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
      .map(function (x) {
        return x[Math.floor(Math.random() * x.length)];
      })
      .join("");
    $("#modal-password").val(randPassword);
    $("#add-password").val(randPassword);
  });

  $('[name="save-password"]').click(function () {
    var id = parseInt($("#modal-id").val());
    var password = $("#modal-password").val();
    $("#password").val("");
    if (password == "") {
      return false;
    }
    var settings = {
      url: apiUrl + "/proctor/update", //
      method: "PUT",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        proctor_id: id,
        password: password,
      }),
    };

    $.ajax(settings)
      .done(function (response) {
        console.log(response);
        if (response.message) {
          alert("New password is updated!\nTake Note: " + password);
        } else {
          alert("Process is ERROR!");
        }
      })
      .fail(function () {
        alert("Process is ERROR!");
      });
  });

// Add new user
  $('[name="add-user"]').click(function () {
    $("#add-displayname").val('');
    $("#add-fromfaculty").val('');
    $("#add-username").val('');
    $("#add-password").val('');
    
    var settings = {
      url: apiUrl + "/manager/role/", //
      method: "GET",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    };
    //   console.log(settings)
    $.ajax(settings)
      .done(function (response) {
        var options;
        response.data.forEach((role) => {
          options +=
            '<option value="' + role.role_id + ' ">' +
            role.role_name + "</option>";
        });
        $("#add-role").html(options);
        $('#add-role option:contains(Proctor)').attr('selected', true)
      })
      .fail(function () {
        alert("Process is ERROR!");
      });

  });



  $('[name="submit-add-user"]').click(function () {
    var displayname = $("#add-displayname").val();
    var fromfaculty = $("#add-fromfaculty").val();
    var username = $("#add-username").val();
    var password = $("#add-password").val();
    var role_id = parseInt($("#add-role").val());

    if (!username || !password) {
      alert("Error! Username and Password can not be empty")
      return false;
    }
    var settings = {
      url: apiUrl + "/register", //
      method: "POST",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        name: displayname,
        from_faculty: fromfaculty,
        username: username,
        password: password,
        role_id: role_id,
      }),
    };
    console.log(settings);
    $.ajax(settings)
      .done(function (response) {
        console.log(response);
        if (response.message) {
          location.reload();
        } else {
          alert("Process is ERROR!");
        }
      })
      .fail(function () {
        alert("Process is ERROR!");
      });
  });
  

  // Edit user profile
  $('[name="edit-user"]').click(function () {
    if (typeof $(this).data("id") !== "undefined") {
      var id = $(this).data("id");
    } else {
      return false;
    }

    var settings = {
      url: apiUrl + "/manager/role/", //
      method: "GET",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    };
    //   console.log(settings)
    $.ajax(settings)
      .done(function (response) {
        var options;
        response.data.forEach((role) => {
          options +=
            '<option value="' +
            role.role_id +
            '">' +
            role.role_name +
            "</option>";
        });
        $("#edit-role").html(options);
        $("#add-role").html(options);
        $('#edit-role option:contains(Proctor)').attr('selected', true)
      })
      .fail(function () {
        alert("Process is ERROR!");
      });

    $.getJSON(apiUrl + "/proctordetail/" + id, function (result) {
      var user = result.data[0];
      $("#edit-id").val(user.proctor_id);
      $("#edit-displayname").val(user.name);
      $("#edit-fromfaculty").val(user.from_faculty);
      $("#edit-username").val(user.username);
      $('#edit-role option:contains(' + user.role_name + ')').attr('selected', true)
    });
  });

  $('[name="update-user"]').click(function () {
    var id = parseInt($("#edit-id").val());
    var displayname = $("#edit-displayname").val();
    var fromfaculty = $("#edit-fromfaculty").val();
    var username = $("#edit-username").val();
    var role_id = parseInt($("#edit-role").val());

    if (!id) {
      return false;
    }
    var settings = {
      url: apiUrl + "/proctor/update", //
      method: "PUT",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        proctor_id: id,
        name: displayname,
        from_faculty: fromfaculty,
        username: username,
        role_id: role_id,
      }),
    };
    console.log(settings);
    $.ajax(settings)
      .done(function (response) {
        console.log(response);
        if (response.message) {
          location.reload();
        } else {
          alert("Process is ERROR!");
        }
      })
      .fail(function () {
        alert("Process is ERROR!");
      });
  });

  // Delete user
  $('[name="delete-user"]').click(function () {
    $("#delete-alert").html(null).hide()
    $("#delete-id").val(null)
    if (typeof $(this).data("id") !== "undefined") {
      var id = $(this).data("id");
      $("#delete-id").val(id)
    } else {
      return false;
    }

    // /api/sessionproctors/5
    $.getJSON(apiUrl + "/sessionproctors/" + id, function (result) {
      if (result.data != null) {
        var msg =
          '<div class="">ບັນຊີທີ່ທ່ານຕ້ອງການລຶບ ມີເປັນກໍາມະການຍາມຢູ່:</div>';
        msg += "<ul>";
        result.data.forEach((session, i) => {
          msg += "<li>" + dateFormat(new Date(session.date)) + ", ຫ້ອງ " + session.room_name + ", " + session.session + "</li>";
        });
        msg += "</ul>";
        // console.log(msg);
        $("#delete-alert").html(msg).show()
      }
    });
  });

  $('[name="comfirm-delete"]').click(function () {
    var id = parseInt($("#delete-id").val());
    var settings = {
      url: apiUrl + "/proctor/delete/" + id, //
      method: "DELETE",
      timeout: 0,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    };
    // console.log(settings);
    $.ajax(settings)
      .done(function (response) {
        console.log(response);
        if (response.message) {
          location.reload();
        } else {
          alert("Process is ERROR 0!");
        }
      })
      .fail(function () {
        alert("Process is ERROR 1!");
      });

  })
  
});

function dateFormat(inputDate) {
  let date, month, year;

  date = inputDate.getDate();
  month = inputDate.getMonth() + 1;
  year = inputDate.getFullYear();

  date = date.toString().padStart(2, "0");
  month = month.toString().padStart(2, "0");

  return date + "/" + month + "/" + year;
}

  
