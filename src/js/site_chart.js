var ctxB = document.getElementById("roomchart").getContext("2d");
var Chart = new Chart(ctxB, {
  type: "bar",
  data: {
    labels: [],
    datasets: [
      {
        label: "ທັງໝົດ",
        data: [],
        fill: false,
        backgroundColor: [], //["rgba(54, 162, 235, 0.2)"],
        borderColor: [], //["rgb(54, 162, 235)"],
        borderWidth: 1,
      },
      {
        label: "ເຂົ້າມາເສັງ",
        data: [],
        fill: false,
        backgroundColor: [], //["rgba(255, 99, 132, 0.2)"],
        borderColor: [], //["rgb(255, 99, 132)"],
        borderWidth: 1,
      },
    ],
  },
  options: {
    indexAxis: "x",
    scales: {
      y: {
        beginAtZero: true,
      },
    },
    plugins: {
      legend: { display: false },
    },
    maintainAspectRatio: false,
    responsive: true,
  },
});

function updateChart(url) {
  $.getJSON(url, function (result) {
    console.log(result);
    if (!result.data) {
      return false
    }
    var xValue = [];
    var yValue1 = [];
    var yValue2 = [];
    var gcolor1 = [];
    var gcolor2 = [];
    var bcolor1 = [];
    var bcolor2 = [];
    result.data.forEach((col, i) => {
      xValue[i] = col.room_name;
      yValue1[i] = col.total;
      yValue2[i] = col.attendance;
      gcolor1[i] = "rgba(54, 162, 235, 0.2)";
      gcolor2[i] = "rgba(255, 99, 132, 0.2)";
      bcolor1[i] = "rgb(54, 162, 235)";
      bcolor2[i] = "rgb(255, 99, 132)";
    });

    Chart.data.labels = xValue;
    Chart.data.datasets[0].data = yValue1;
    Chart.data.datasets[0].backgroundColor = gcolor1;
    Chart.data.datasets[0].borderColor = bcolor1;
    Chart.data.datasets[1].data = yValue2;
    Chart.data.datasets[1].backgroundColor = gcolor2;
    Chart.data.datasets[1].borderColor = bcolor2;
    Chart.update();
  });
  
}
// console.log(apiUrl)
var api_url = apiUrl + "/sitestatistic/" + data.exam_site_id + "/" + data.date + "/" + data.session;

updateChart(api_url);

setInterval(updateChart, 10 * 1000, api_url);
