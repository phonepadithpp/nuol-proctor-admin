<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token']) {
    @header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}

if (@$_SESSION['role_id'] != 1) {
    @header("Location: main.php");
    echo "<script>location='main.php'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css?df" rel="stylesheet">
    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i> <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php"><i class="fa fa-unlock-alt"></i> ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row g-3">
            <div class="col m-5">
                <p>
                <h3>Import ຕາງຕະລາງລາຍຊື່ກໍາມະການຍາມຫ້ອງສອບເສັງ</h3>
                ຕົວຢ່າງຮູບແບບຟາຍ Excel ທີ່ສະໜັບສະໜູນ <a href="temple_import_ກຳມະການ.xlsx"><i class="fa fa-file-excel-o"></i> temple_import_ກຳມະການ.xlsx</a></p>
                <form action="excelUpload.php" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="fileupload" id="customFileInput" aria-describedby="customFileInput" required>
                            <label class="custom-file-label" for="customFileInput">ເລືອກຟາຍ</label>
                        </div>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" id="FileImport">Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row my=3 pb-3 pl-5">
            <?php
            if (@$_SESSION['response']['log']) {
                echo "<strong>Process Resault:</strong><br>";
                foreach ($_SESSION['response']['log'] as $log) {
                    echo "<div class=\"t\">$log</div>";
                }
                unset($_SESSION['response']);
            } else if (@$_SESSION['response']['error']) {
                echo "<strong>Import Fail: ";
                echo $_SESSION['response']['error'];
                echo "</strong><br>";
                unset($_SESSION['response']);
            }
            ?>
        </div>

        <div class="row d-flex flex-row">
            <hr>
            <footer>
                <div class="col text-center">
                    <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                </div>
                <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
            </footer>
        </div>
    </div>
</body>

</html>
<section>
    <script>
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            var name = document.getElementById("customFileInput").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = name
        })

        $('#FileImport').click(function() {
            $('[type="file"]').on("invalid", function() {
                this.setCustomValidity("ກາລຸນາເລືອກຟາຍທີ່ຕ້ອງການ");
            }).on("input", function() {
                this.setCustomValidity("");
            })
        })
    </script>
</section>