<?php
if (@!session_id()) session_start();

if (@!$_SESSION['token']) {
    @header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}
if (@$_SESSION['role_id'] != 1) {
    @header("Location: main.php");
    echo "<script>location='main.php'</script>";
    exit;
}

if (empty($_FILES['fileupload'])) {
    $_SESSION['response']['error'] = "Upload file Error!";
    @header("Location: importexcel.php");
    echo "<script>location='importexcel.php'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";
require('libs/spreadsheet-reader-master/php-excel-reader/excel_reader2.php');
require('libs/spreadsheet-reader-master/SpreadsheetReader.php');

$filename = $_FILES['fileupload']['name'];
$tmp = $_FILES['fileupload']['tmp_name'];
$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
$allowed = array('xlsx', 'xls');

if (!in_array($ext, $allowed)) {
    $_SESSION['response']['error'] = "File Extention \".$ext\" is not allowed";
    @header("Location: importexcel.php");
    echo "<script>location='importexcel.php'</script>";
    exit;
}

if (is_uploaded_file($tmp)) {
    if (!is_writable('tmp/')) {
        $_SESSION['response']['error'] = "Failed to upload file: no permission to write file to system directory";
        @header("Location: importexcel.php");
        echo "<script>location='importexcel.php'</script>";
        exit;
    }
    $uploadFilePath = 'tmp/' . basename($filename);
    @move_uploaded_file($tmp, $uploadFilePath);

    $StartMem = memory_get_usage();
    $Reader = @new SpreadsheetReader($uploadFilePath);
    $BaseMem = memory_get_usage();
    $Sheet = $Reader->Sheets();
    $Reader->ChangeSheet(0);
    $date = '';
    $error_log = [];
    $data = [];

    foreach ($Reader as $i => $Row) {

        if ($i == 1) {
            if (preg_match("/\(([0-9\-]+)\)/i", $Row[0], $match)) {
                $date = @$match[1];
            }
        }
        if ($i == 2) {
            $site_name = trim($Row[0]);
            if (preg_match("/\s/", $site_name, $match)) {
                $data['site_name'] = trim(substr($site_name, strpos($site_name, $match[0])));
            }

        }

        if ($i > 3) {
            // print_r($Row);
            if ($Row[2] != '') {
                $data['data'][] = array("date" => $date, "room_name" => trim($Row[1]), "proctor" => trim($Row[2]), "faculty" => trim($Row[3]), "username" => trim($Row[2]), "password" => "12345678");
            } else {
                $error_log[] = array("room_name" => trim($Row[1]), "proctor" => trim($Row[2]), "faculty" => trim($Row[3]));
            }
            if ($Row[4] != '') {
                $data['data'][] = array("date" => $date, "room_name" => trim($Row[1]), "proctor" => trim($Row[4]), "faculty" => trim($Row[5]), "username" => $Row[4], "password" => "12345678");
            } else {
                $error_log[] = array("room_name" => trim($Row[1]), "proctor" => trim($Row[4]), "faculty" => trim($Row[5]));
            }
        }
    }
    // echo $uploadFilePath;
    unlink($uploadFilePath);
    
    // echo $apiUrl = "127.0.0.1:8000/api";
    $url = $apiUrl . "/proctor/import";
    $data = json_encode($data);

    $response = CallAPI('POST', $url, $data);
    $result = json_decode($response, true);
    if (empty($result)) {
        if (empty($response)) {
            $_SESSION["response"]['error'] = "Can not connect to database: the connection error";
        } else if ($response == "Unauthorized") {
            @header("Location: login.php");
            echo "<script>location='login.php'</script>";
            exit;
        } else {
            $_SESSION["response"]['error'] = $response;
        }
    } else {
        $_SESSION["response"] = json_decode($response, true);
    }
    
    // print_r($_SESSION["response"]);
    @Header("Location: importexcel.php");
    echo "<script>location='importexcel.php'</script>";
}
echo "<p>UNKNOW ERROR</p>";