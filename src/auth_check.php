<?php
if (@!session_id()) session_start();
require_once "api_setting.php";
require_once "functions.php";

if (@!$_POST['verify_token'] | (@$_POST['verify_token'] != $_SESSION['verify_token'])) {
    // @header("Location: login.php");
    // echo "<script>location='login.php'</script>";
    exit;
}
unset($_SESSION['verify_token']);

$data = array("username"=>strtolower($_POST['username']), "password"=>$_POST['password']);
$data = json_encode($data);

$url = $apiUrl."/login";

$response = json_decode(CallAPI('POST', $url, $data), true);
if (!$response) {
    CallAPI('POST', $apiUrl."/logging", json_encode(array("event"=>"Login failed: username=$_POST[username]&password=$_POST[password]", "proctor_id"=>0)));
    @header("Location: login.php?error=101");
    echo "<script>location='login.php?error=101'</script>";
    exit;
}

if (@$response['token']) {
    $_SESSION['token'] = @$response['token'];
    $data = token_decode($response['token']);
    $_SESSION['username'] = @$data['username'];
    $_SESSION['displayname'] = @$data['name'];
    $_SESSION['user_id'] = @$data['user_id'];
    $_SESSION['role_id'] = @$data['role_id'];
    if ($data['role_id']==1 | $data['role_id']==3) {
        CallAPI('POST', $apiUrl."/logging", json_encode(array("event"=>"Logged in success: OK", "proctor_id"=>@$data['user_id'])));
        @header("Location: main.php");
        echo "<script>location='main.php'</script>";
        exit;
    } else {
        CallAPI('POST', $apiUrl."/logging", json_encode(array("event"=>"Login failed: not authorized user role tried to logging", "proctor_id"=>@$data['user_id'])));
        @header("Location: login.php?error=100");
        echo "<script>location='login.php?error=100'</script>";
        exit;
    }
}
@header("Location: login.php?error=100");
echo "<script>location='login.php?error=100'</script>";
exit;
