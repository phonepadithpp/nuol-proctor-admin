<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token'] || @!$_GET['id']) {
    @header("Location: login.php");
    echo "<script>location='login.php'</script>";
    exit;
}

require_once "api_setting.php";
require_once "functions.php";


$url = $apiUrl . "/sitesession/" . $_GET['id'];

$response = json_decode(CallAPI('GET', $url), true);
if (!$response) {
    echo "Can not connect to database: the connection error";
    exit;
}
$data = @$response['data'] ? $response['data'][0] : array();

$uerl1 = $apiUrl . "/sitedetails/" . $_GET['id'];
$response1 = json_decode(CallAPI('GET', $uerl1), true);

$rooms = @$response1['Data'][0]['Rooms'];
// var_dump($rooms);
$data1 = [];
foreach ((array)$rooms as $room) {
    $data1[$room['room_id']] =  $room['room_name'];
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css" rel="stylesheet">
    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
    <script>
        <?php
        $date = @$_GET['d'] ? $_GET['d'] : date("Y-m-d");
        $session = date("G") > 12 ? "ພາກບ່າຍ" : "ພາກເຊົ້າ";
        $session = @$_GET['s'] ? $_GET['s'] : $session;
        ?>
        var data = {
            exam_site_id: <?php echo $data['site_id'] ?>,
            date: "<?php echo $date ?>",
            session: "<?php echo $session ?>"
        }
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                        </li>
                    <?php } ?>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                        </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i> <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php"><i class="fa fa-unlock-alt"></i> ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="navbar navbar-expand-sm">
            <a href="main.php" class="nav-link">ໜ້າຫຼັກ</a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="#" class="nav-link text-body">ສູນສອບເສັງ: <?php echo $data['site_name'] ?></a><i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
        <div class="p-5 my-4 bg-light rounded-3 text-center" id="chartContainer" style="max-height: 500px;">
            <h4>ສະຖິຕິນັກສອບເຂົ້າເສັງຕົວຈິງ ຂອງສູນສອບເສັງ <span class="badge bg-info"><?php echo $data['site_name'] ?></span> (Live)</H4>
            <canvas id="roomchart"></canvas>
        </div>
        <br>
        <div class="row g-3 mb-5 mt-4 mt-sm-1 mt-lg-0">
            <div class="d-flex flex-row justify-content-center mt-auto">
                <?php
                foreach ((array)$data['site_session'] as $i => $item) {
                    $item['date'] = $item['date'] ? strtotime(@$item['date']) : "";
                ?>

                    <div class="col-md-3 col-lg-3 col-xl-2 m-2">

                        <a href="viewsite.php?id=<?php echo @$data['site_id'] ?>&d=<?php echo date("Y-m-d", @$item['date']) ?>&s=<?php echo @$item['session'] ?>" class="btn 
                        <?php
                        if ($date == date("Y-m-d", @$item['date']) && $session == $item['session']) {
                            echo "btn-success";
                        } else {
                            echo "btn-outline-success";
                        }
                        ?>
                         card-anime">
                            <h4><?php echo date("d/m/Y", @$item['date']) ?><br><?php echo @$item['session'] ?></h4>
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class=""><strong>ຫ້ອງສອບເສັງ:</strong>
            <ul class="nav">
                <?php
                foreach ($data1 as $id => $room) {
                ?>
                    <li class="nav-item">
                        <a class="btn btn-outline-primary m-1 sm" href="viewroom.php?id=<?php echo @$id ?>"><?php echo @$room ?></a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>

        <div class="row d-flex flex-row">
            <hr>
            <footer>
                <div class="col text-center">
                    <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                </div>
                <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
            </footer>
        </div>
    </div>
</body>

</html>
<section>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="js/site_chart.js?dfdf"></script>
    <script src="js/custom.js"></script>
</section>