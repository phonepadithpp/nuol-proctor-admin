<?php
if (@!session_id()) session_start();
if (@!$_SESSION['token']) {
    @header("Location: login.php");
    exit;
}

if (@!$_GET['id']) {
    @header("Location: reportview.php");
    echo "<script>location='reportview.php'</script>";
    exit;
}
require_once "api_setting.php";
require_once "functions.php";

// echo "<center><h2>Under Construction!</h2></center>";
// exit;

// echo $apiUrl = "127.0.0.1:8000/api";
$url = $apiUrl . "/reportroom/" . @$_GET['id'];

$response = json_decode(CallAPI('GET', $url), true);
if (!$response) {
    echo " Can not connect to database: the connection error";
    exit;
}
$data = @$response['data'] ? $response['data'] : array();
// echo "<pre>";
// print_r($data);
// exit;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>ມະຫາວິທະຍາໄລແຫ່ງຊາດ</title>
    <link rel="icon" type="image/png" href="nuol.png" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <link href="css/custom.css?df" rel="stylesheet">
    <script>
        let token = "<?php echo $_SESSION['token'] ?>"
        let apiUrl = "<?php echo $apiUrl ?>"
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container-fluid">
            <div class="navbar-brand">ຕິດຕາມການສອບເສັງ</div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="main.php">ໜ້າຫຼັກ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="reportview.php">ລາຍງານ</a>
                    </li>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="usermanager.php">ບັນຊີຜູ້ໃຊ້</a>
                        </li>
                    <?php } ?>
                    <?php if (@$_SESSION['role_id'] == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="importexcel.php">Import ຕາຕະລາງກໍາມະການ</a>
                        </li>
                    <?php } ?>
                    <!-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </li> -->
                </ul>

                <!-- <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form> -->

                <ul class="navbar-nav mb-lg-0 nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i> <?php echo @$_SESSION['displayname'] ? $_SESSION['displayname'] : $_SESSION['username'] ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" href="changecurrentpassword.php"><i class="fa fa-unlock-alt"></i> ປ່ຽນລະຫັດຜ່ານ</a></li>
                            <!-- <li><a class="dropdown-item" href="#">Another action</a></li> -->
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ອອກຈາກລະບົບ</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="navbar navbar-expand-sm">
            <a href="reportview.php" class="nav-link">ກຸມວິຊາສອບເສັງ</a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="reportview-site.php?id=<?php echo $data['group_id'] ?>" class="nav-link"><?php echo @$data['group_name'] ?></a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="reportview-site-room.php?id=<?php echo $data['site_id'] ?>" class="nav-link">ສູນສອບເສັງ: <?php echo @$data['site_name'] ?></a><i class="fa fa-angle-right" aria-hidden="true"></i>
            <a href="#" class="nav-link text-body">ລາຍງານຫ້ອງ: <?php echo @$data['room_name'] ?> (<?php echo @$data['session'] ?>)</a><i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
        <div class="row g-3 d-flex justify-content-center">
            <h4 class="text-center py-3"><strong><u>ລາຍງານສະພາບການສອບເສັງ</u></strong></h4>
            <div class="m-0">ຄັ້ງວັນທີ: <strong><?php echo @date("d/m/Y", strtotime($data['session_date'])) ?> (<?php echo @$data['session'] ?>)</strong></div>
            <div class="m-0">ຫ້ອງ: <strong><?php echo @$data['room_name'] ?></strong></div>
            <div class="m-0">ສູນສອບເສັງ: <?php echo @$data['site_name'] ?></div>
            <div class="m-0">ກຸມວິຊາສອບເສັງ: <strong><?php echo @$data['group_name'] ?></strong></div>
            <div class="m-0">ກໍາມະການຍາມຫ້ອງສອບ:
                <?php
                if ($data["proctors"]) {
                    foreach ((array)$data["proctors"] as $k => $val) {
                        echo "<span>";
                        echo (++$k) . ". $val[proctor_name]";
                        echo "</span>, ";
                    }
                } else {
                    echo "...................................................................";
                }

                ?>
            </div>
            <div>
                <strong>ສະພາບລວມ:</strong> <span class="sm-w-100">ທັງໝົດ: <?php echo @$data['applicant_no'] ?> ຄົນ / </span><span>ຂາດ: <?php echo @count((array)$data['absent']) ?> ຄົນ / </span><span>ຜິດວິໄນ: <?php echo @count((array)$data['rule_breaker']) ?> ຄົນ </span>
                <span class="float-end"><small>
                        <?php
                        if (@$data['CreateDate']) {
                            echo @date("d/m/Y H:i:s", @strtotime($data['CreateDate']));
                        } ?>
                    </small>
                </span>

                <div class="border p-3 bg-light mt-2 mb-5" style="min-height: 80px;">
                    <?php
                    echo @$data['detail'] ? $data['detail'] : ".....";
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="text-center"><strong>ລາຍຊື່ນັກສອບເສັງຂາດ</strong></div>
                    <div>
                        <table class="table table-bordered">
                            <thead>
                                <th class="text-center">ລ/ດ</th>
                                <th>ຊື່ນາມສະກຸນ</th>
                                <th class="text-center">ລະຫັດສອບເສັງ</th>
                            </thead>
                            <tbody>
                                <?php
                                if (@$data['absent']) {
                                    foreach ((array)$data['absent'] as $i => $val) {
                                ?>
                                        <tr>
                                            <td class="text-center"><?php echo ++$i ?></td>
                                            <td><?php echo $val['full_name'] ?></td>
                                            <td class="text-center"><?php echo $val['seat_no'] ?></td>
                                        </tr>
                                <?php
                                    }
                                } else {
                                    echo "<tr><td colspan=\"3\" class=\"text-center\">ບໍ່ມີນັກສອບເສັງຂາດ</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col">
                    <div class="text-center"><strong>ລາາຍຊື່ນັກສອບເສັງຜິດວິໄນ</strong></div>
                    <div>
                        <table class="table table-bordered">
                            <thead>
                                <th class="text-center">ລ/ດ</th>
                                <th>ຊື່ນາມສະກຸນ</th>
                                <th class="text-center">ລະຫັດສອບເສັງ</th>
                            </thead>
                            <tbody>
                                <?php
                                if ($data['rule_breaker']) {
                                    foreach ((array)$data['rule_breaker'] as $i => $val) {
                                ?>
                                        <tr>
                                            <td class="text-center"><?php echo ++$i ?></td>
                                            <td><?php echo $val['full_name'] ?></td>
                                            <td class="text-center"><?php echo $val['seat_no'] ?></td>
                                        </tr>
                                <?php
                                    }
                                } else {
                                    echo "<tr><td colspan=\"3\" class=\"text-center\">ບໍ່ມີນັກສອບເສັງຜິດວິໄນ</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <p>&nbsp;</p>
        <div class="row d-flex flex-row">
            <hr>
            <footer>
                <div class="col text-center">
                    <p>2022 &COPY; ມະຫາວິທະຍາໄລແຫ່ງຊາດ</p>
                </div>
                <!-- <div class="col-md-6 text-md-end">
                    <a href="#" class="text-dark">Terms of Use</a>
                    <span class="text-muted mx-2">|</span>
                    <a href="#" class="text-dark">Privacy Policy</a>
                </div> -->
            </footer>
        </div>
    </div>
</body>

</html>
<section>
    <script src="js/custom.js"></script>
</section>